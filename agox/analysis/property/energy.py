from typing import Literal

import numpy as np

from agox.analysis.search_data import SearchData

from .property import ArrayPropertyData, Property


class EnergyProperty(Property):
    def __init__(self, 
                time_axis: np.ndarray = None, 
                time_unit: Literal["s", "m", "h", "d", "y"] = "s", 
                cost_factor: int = 1) -> None:
        """
        Get the energy of the system as a np.array of shape [restarts, iterations].
        """
        if time_axis is None:
            time_axis = "indices"

        if time_axis not in ["indices", "iterations", "time"]:
            raise ValueError("time_axis must be one of ['indices', 'iterations', 'time']")

        self.time_axis = time_axis
        self.time_unit_symbol = time_unit
        
        if time_unit == "s":
            self.unit = 1
        elif time_unit == "m":
            self.unit = 60
        elif time_unit == "h":
            self.unit = 3600
        elif time_unit == "d":
            self.unit = 86400        
        elif time_unit == "y":
            self.unit = 31536000
        else:
            raise ValueError("time_unit must be one of ['s', 'm', 'h', 'd']")
        
        self.cost_factor = cost_factor

    def get_time_axis(self, search_data: SearchData) -> str:
        axis_name = self.time_axis.capitalize()

        if self.time_axis == "indices":
            indices = search_data.get_all("indices", fill=np.nan)
        elif self.time_axis == "iterations":
            indices = search_data.get_all("iterations", fill=np.nan)
        else:
            indices = search_data.get_all("times", fill=np.nan) / self.unit * self.cost_factor
            if self.cost_factor == 1:
                axis_name = axis_name + f" [{self.time_unit_symbol}]"
            else:
                axis_name = f"CPU cost [{self.time_unit_symbol}]"

        if self.time_axis in ["indices", "iterations"]:
            axis_name = f"{axis_name} [#]"

        return axis_name, indices

    def compute(self, search_data: SearchData) -> np.array:
        """
        Get the energy of the system as a np.array of shape [restarts, iterations].
        """

        axis_name, indices = self.get_time_axis(search_data)

        energy = ArrayPropertyData(
            data=search_data.get_all("energies", fill=np.inf),
            name="Energy",
            shape=("Restarts", axis_name),
            array_axis=(search_data.get_all_identifiers(), indices),
        )
        return energy
