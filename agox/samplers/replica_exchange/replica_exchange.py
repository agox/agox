from typing import List, Literal, Optional

import numpy as np
from ase.calculators.singlepoint import SinglePointCalculator

from agox import State
from agox.candidates import Candidate
from agox.models import Model, ModelBaseClass
from agox.observer import Observer
from agox.samplers import SamplerBaseClass
from agox.samplers.replica_exchange import RateTracker


class ReplicaExchangeSampler(SamplerBaseClass):
    name = "RepliceExchangeSampler"

    """
    Replica exchange sampler.

    Parameters
    ----------
    sample_size : int
        Number of members in the sample.
    swap : str
        Method for swapping members. Options are 'up' or 'down'.
    temperatures: List[float]
        List of temperatures to use.
    t_max : float
        Maximum temperature.
    t_min : float
        Minimum temperature.
    swap_interval : int
        Number of iterations between swap attempts.
    swap: Literal["up", "down"]
        Method for swapping members. Options are 'up' or 'down'.
    always_accept : bool
        If True, always accept the new candidate.
    flush_prob : float
        Probability of flushing a member of the sample - to replace it by a new candidate.
        Happens after the model has been updated.
    gets : dict
        Dictionary with the key to get the candidates.
    sets : dict
        Dictionary with the key to set the candidates.
    """
    def __init__(
        self,
        model: Model = None,
        t_min: float = None,
        t_max: float = None,
        sample_size: int = 5,
        swap: Literal["up", "down"] = "up",
        temperatures: Optional[np.ndarray] = None,
        swap_interval: int = 10,
        always_accept: bool = False,
        flush_prob: float = 0.0,
        gets: dict = {"get_key": "candidates"},
        sets: dict = {"set_key": "candidates"},
        **kwargs,
    ) -> None:
        super().__init__(gets=gets, sets=sets, **kwargs)

        self.sample_size = sample_size
        self.swap_interval = swap_interval
        self.always_accept = always_accept
        self.flush_prob = flush_prob

        # Temperature logic:
        # Check that they are not all not none:
        if t_max is not None and t_min is not None and temperatures is not None:
            raise ValueError("Either t_max and t_min or temperatures must be provided, not both.")
        elif t_max is not None and t_min is not None:  # If both are provided, generate the temperatures.
            self.temperatures = np.geomspace(t_min, t_max, sample_size)
        elif temperatures is not None:
            if len(temperatures) != sample_size:
                raise ValueError(
                    f"Length of temperatures ({len(temperatures)}) does not match sample size ({sample_size})."
                )
            else:
                self.temperatures = temperatures
        else:
            raise ValueError("Either t_max and t_min or temperatures must be provided.")

        self.reset_observer()  # We remove observers added by the base-class.

        self.add_observer_method(
            self.update_sampler_energies,
            gets=self.gets[0],
            sets=self.sets[0],
            order=self.order[0],
            handler_identifier="model",
        )

        self.add_observer_method(
            self.setup_sampler,
            gets=self.gets[0],
            sets=self.sets[0],
            order=self.order[0],
            handler_identifier="AGOX",
        )

        self.tracker = RateTracker(sample_size)

        if swap == "up":
            self.swap = self.swap_up
        elif swap == "down":
            self.swap = self.swap_down
        else:
            raise ValueError(f"Swap method '{swap}' not recognized. Use 'up' or 'down'.")

        if model is not None:
            self.attach_to_model(model)

    @Observer.observer_method
    def update_sampler_energies(self, model: Model, state: State) -> None:
        self.previous_sample = self.sample.copy()

        if self.flush_prob > 0.0:
            for i in range(len(self.sample)):
                if np.random.rand() < self.flush_prob:
                    self.sample[i] = None
        for parent in self.sample:
            if parent is None:
                continue
            E = model.predict_energy(parent) # noqa: N806
            results = {"energy": E}
            parent.calc = SinglePointCalculator(parent, **results)

    @Observer.observer_method
    def setup_sampler(self, state: State) -> None:
        if self.do_check():
            evaluated_candidates = state.get_from_cache(self, self.get_key)
            evaluated_candidates = list(filter(None, evaluated_candidates))
            if len(evaluated_candidates) > 0:
                self.setup(evaluated_candidates)

        # Overwrite the candidates on the cache as the sampler may have updated meta information.
        state.add_to_cache(self, self.set_key, evaluated_candidates, mode="w")

    def setup(self, evaluated_candidates: List[Candidate]) -> None:
        if len(self.sample) == 0:
            self.setup_when_empty(evaluated_candidates)
            return

        if None in self.sample:
            self.setup_when_flushed(evaluated_candidates)
            return

        self.update_sample(evaluated_candidates)

        iteration = self.get_iteration_counter()
        if self.decide_to_swap(iteration):
            self.swap()

        if iteration % 1 == 0:
            self.report_statistics()

    def update_sample(self, evaluated_candidates: List[Candidate]) -> None:
        for candidate in evaluated_candidates:
            sample_index = candidate.get_meta_information("walker_index")
            if self.sample[sample_index] is None:
                parent = None
                accepted = True
            else:
                parent = self.sample[sample_index]
                temperature = self.temperatures[sample_index]
                # Determine if the candidate gets accepted:
                accepted = self.metropolis_check(candidate, parent, temperature)

            self.tracker.update_acceptance(sample_index, int(accepted))
            if accepted:
                self.sample[sample_index] = candidate

            candidate.add_meta_information("accepted", accepted)

    def metropolis_check(self, candidate: Candidate, parent: Candidate, temperature: float) -> bool:
        if self.always_accept:
            return True
        E_candidate = self.get_potential_energy(candidate) # noqa
        E_parent = self.get_potential_energy(parent) # noqa
        if E_candidate < E_parent:
            accepted = True
        else:
            P = np.exp(-(E_candidate - E_parent) / temperature) # noqa
            accepted = P > np.random.rand()
        return accepted

    def swap_check(self, i: int , j: int) -> bool:
        """
        Check if we should swap the i-th and j-th member of the sample.

        Parameters
        ----------
        i: int
            Index of the first member.
        j: int
            Index of the second member.

        Returns
        -------
        bool
            True if the swap is accepted, False otherwise.
        """
        E_i = self.get_potential_energy(self.sample[i]) # noqa: N806
        E_j = self.get_potential_energy(self.sample[j]) # noqa: N806
        beta_i = 1 / self.temperatures[i]
        beta_j = 1 / self.temperatures[j]

        P = np.min([1, np.exp((beta_i - beta_j) * (E_i - E_j))]) # noqa: N806

        return P > np.random.rand()

    def swap_up(self) -> None:
        self.writer("Swapping in 'up' mode")

        # Run over the sample starting from the bottom:
        for i in range(len(self.sample) - 1):
            swap_bool = self.swap_check(i, i + 1)

            self.tracker.update_swap_up(i, int(swap_bool))
            self.tracker.update_swap_down(i + 1, int(swap_bool))

            if swap_bool:
                self.sample[i], self.sample[i + 1] = self.sample[i + 1], self.sample[i]

    def swap_down(self) -> None:
        self.writer("Swapping in 'down' mode")

        # Run over the sample starting from the highest temperature:
        for i in range(len(self.sample) - 1, 0, -1):
            swap_bool = self.swap_check(i, i - 1)

            self.tracker.update_swap_up(i - 1, int(swap_bool))
            self.tracker.update_swap_down(i, int(swap_bool))

            if swap_bool:
                self.sample[i], self.sample[i - 1] = self.sample[i - 1], self.sample[i]

    def decide_to_swap(self, iteration_counter: int) -> bool:
        return iteration_counter % self.swap_interval == 0

    def setup_when_empty(self, evaluated_candidates: List[Candidate]) -> None:
        replace = len(evaluated_candidates) < self.sample_size
        indices = np.arange(len(evaluated_candidates))
        sample_indices = np.random.choice(indices, size=self.sample_size, replace=replace)
        self.sample = [evaluated_candidates[i] for i in sample_indices]

        for candidate in evaluated_candidates:
            candidate.add_meta_information("accepted", True)

    def setup_when_flushed(self, evaluated_candidates: List[Candidate]) -> None:
        walker_indices = [candidate.get_meta_information("walker_index") for candidate in evaluated_candidates]
        for i, sample_member in enumerate(self.sample):
            if sample_member is None:
                if i in walker_indices:
                    cand_index = walker_indices.index(i)
                    self.sample[i] = evaluated_candidates[cand_index]
                else:
                    self.sample[i] = self.previous_sample[i]
        for candidate in evaluated_candidates:
            candidate.add_meta_information("accepted", True)

    def report_statistics(self) -> None:
        def float_format(value: float) -> str:
            return f"{value:.2f}"

        columns = ["Member", "Temperature", "Energy", "Accept", "Up", "Down"]
        rows = []
        for i in range(self.sample_size):
            swap_up = float_format(self.get_swap_up_rate(i))
            swap_down = float_format(self.get_swap_down_rate(i))
            acceptance = float_format(self.get_acceptance_rate(i, start=-10))
            energy = float_format(self.sample[i].get_meta_information("pt_energy") or 0)
            rows.append([str(i), float_format(self.temperatures[i]), energy, acceptance, swap_up, swap_down])

        self.writer.write_table(table_columns=columns, table_rows=rows, expand=True)

    def get_acceptance_rate(self, index: int, start: Optional[int] = None, stop: Optional[int] = None, step: Optional[int] = None) -> float:
        return self.tracker.get_acceptance_rate(index, start, stop, step)

    def get_swap_up_rate(self, index: int, start: Optional[int] = None, stop: Optional[int] = None, step: Optional[int] = None) -> float:
        return self.tracker.get_swap_up_rate(index, start, stop, step)

    def get_swap_down_rate(self, index: int, start: Optional[int] = None, stop: Optional[int] = None, step: Optional[int] = None) -> float:
        return self.tracker.get_swap_down_rate(index, start, stop, step)

    def get_potential_energy(self, candidate: Candidate) -> float:
        if hasattr(candidate, "calc"):
            E = candidate.get_potential_energy() # noqa
        else:
            E = 0.0 # noqa

        candidate.add_meta_information("pt_energy", float(E))
        return float(E)

    def attach_to_model(self, model: Model) -> None:
        assert isinstance(model, ModelBaseClass)
        print(f"{self.name}: Attaching to model: {model}")
        self.attach(model)
