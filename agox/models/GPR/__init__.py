from .GPR import GPR
from .sGPR import SparseGPR
from .sGPR_ensemble import SparseGPREnsemble

__all__ = ["GPR", "SparseGPR", "SparseGPREnsemble"]
