Concurrent Tempering
======================

The script below implements a concurrent-tempering basin hopping (PT-BH) algorithm 
where several basin-hopping runs are run concurrently at different temperatures. 
Workers with adjacent temperatures swap their Metropolis accepted candidate.

This is similar to parallel tempering, but differs in how parallelization/concurrency is 
handled. 

- The concurrent tempering version works with multiple indepedent python instances, each acting as a walker and communication is done through a synchronized database. 

.. literalinclude:: ../../../agox/test/run_tests/tests_ct/script_ct.py

.. warning::
   The script showcased here is intended to require little computational effort and therefore uses 
   use an Effective Medium Theory (EMT) potential, which is not suitable for 
   actual studies of materials properties. Please replace the `calculator` with 
   a more accurate potential before using the script for any serious calculations.
   See :ref:`MOD_ALGO` for additional details on how to make such changes.
