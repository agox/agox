Parallel Tempering
======================

In parallel tempering multiple Basin-hopping type walkers concurrently explore 
the PES at different temperatures and interact through swapping of their candidates. 
The parallel tempering implementation in AGOX is designed for exploration in a 
relatively cheap potential, e.g. in a ML potential and parallelism is handled by 
Ray. The ML potential may then be updated based on evaluations of selected configurations 
at the desired level of accuracy (e.g. DFT).

This differs from the Concurrent Tempering implementation where concurrency
is handled by multiple Python instances and communicataion through a concurrent database. 

.. literalinclude:: ../../../agox/test/run_tests/tests_pt/script_pt.py

.. warning::
   The script showcased here is intended to require little computational effort and therefore uses 
   use an Effective Medium Theory (EMT) potential, which is not suitable for 
   actual studies of materials properties. Please replace the `calculator` with 
   a more accurate potential before using the script for any serious calculations.
   See :ref:`MOD_ALGO` for additional details on how to make such changes.
