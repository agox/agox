Bonus topics
========================

.. toctree::
   :maxdepth: 1

   analyzing_databases
   parallelization.rst
   evaluator_callback.rst
