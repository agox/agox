.. _MOD_ALGO:

Modifying algorithms
=====================

AGOX implements a number of modules and ways for these modules to interact. 
Modifications can generally be placed in three catagories; 

.. toctree::
    :maxdepth: 3

    potential
    system




