Installation
============

Requirements
_____________

- Python 3
- pip 
- A C compiler

Consider making sure your version of pip is up to date before installing AGOX

.. code-block:: console

   pip install --upgrade pip

Installation from PyPI
_______________________

The easist way to install AGOX is from PyPI

.. code-block:: console

   pip install agox['full']

This installs the latest stable version of AGOX and all dependencies.

Installation from source
_________________________

To install AGOX clone the repository or download directly from the repository

.. code-block:: console

   git clone https://gitlab.com/agox/agox.git

and install using pip

.. code-block:: console

   pip install agox['full']

For development installing in development mode is beneficial

.. code-block:: console

   pip install -e agox['full']


