Command-line tools
================================

Upon installation AGOX adds a few convenient command-line tools

:code:`agox-convert` can be used to convert AGOX database files to ASE trajectories 

.. code-block:: console

    agox-convert <DB_FILES> -n <TRAJECTORY_NAME>

and :code:`agox-analysis` runs the analysis program on directories containing 
db-files

.. code-block:: console

    agox-analysis -d <DIRECTORIES>

It is also possible to run the analysis using a graph-based comparion by adding the :code:`-uf` flag:

.. code-block:: console

    agox-analysis -d <DIRECTORIES> -uf 

Adding a target structure allows for creating success curves
for finding a specific structure. One can also add the template structure
from the runscript to ignore the template atoms when comparing graphs.

.. code-block:: console

    agox-analysis -d <DIRECTORIES> -uf -target target_structure.traj -template template.traj

If one is interested in extracting a number of unique structures from a directory of databases,
this can be done using the following command:

.. code-block:: console

    agox-graph-sorting -d <DIRECTORIES> -ss 10 -template template.traj

Here, :code:`-ss 10` is the number of unique structures to extract from each folder.
A trajectory file named :code:`combined_sample.traj` will be created in the working directory.