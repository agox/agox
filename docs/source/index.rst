Welcome to AGOX's documentation!
================================

.. image:: https://gitlab.com/agox/agox/badges/dev/pipeline.svg
   :target: https://gitlab.com/agox/agox/pipelines
   :alt: Build status

.. image:: https://gitlab.com/agox/agox/badges/dev/coverage.svg
   :target: _static/htmlcov/
   :alt: Test coverage

.. image:: logos/logo-nickel.gif
   :width: 100%

**AGOX** is a Python package for global optimization of atomistic structures, leveraging state of the art machine learning 
techniques to efficiently solve complicated problems. AGOX supports a variety of algorithms through its modular 
design and is designed to be easily extensible.

Please reach out to us on our GitLab page if you have any questions or suggestions!

.. toctree::
   :maxdepth: 1
   :caption: Contents:


Contents
--------
.. toctree::
   :maxdepth: 1

   installation
   getting_started/getting_started
   algorithms/algorithms
   modifying_algorithms/modifying_algorithms
   agox_framework/agox_framework
   writing_modules/writing_modules   
   generators/generators
   models/models
   bonus_topics/bonus_topics
   command_line_tools/command_line
   references/references

API
---------

.. toctree::
   :maxdepth: 2
   
   autoapi/agox/index

Funding
--------

This work has been supported by funding from the Villum foundation, the Danish National Research Foundation, and is developed by members 
of Center for Interstaller Catalysis (Intercat).

.. list-table::
   :width: 100%
   :class: borderless

   * - .. image:: logos/villum_foundation.png
          :width: 100%
         
     - .. image:: logos/DG_logo.png
          :width: 100%

     - .. image:: logos/INTERCAT_RGB_ONLINE.png
          :width: 100%

.. note:: 

   This documentation is a work in progress!
